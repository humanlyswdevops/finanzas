@extends('layouts.simple.master')

@section('pageTitle', 'Sucursales')

@section('title', 'Sucursales')

@section('css')
@endsection

@section('style')
@endsection

@section('bradcrumb-title')
    <h5>Sucursales</h5>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Confuguraciones</li>
    <li class="breadcrumb-item active">Sucursales</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="col-sm-12">
            <div class="card">
                <div class="ribbon ribbon-bookmark ribbon-secondary">Lista de sucursales</div>
                <div class="card-header p-2">
                    <button class="btn btn-outline-primary btn-sm f-right">
                        Nueva sucursal
                    </button>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-xs table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Razón social</th>
                                    <th>Marca comercial</th>
                                    <th>Estado</th>
                                    <th>Dirección</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $contador = 1;
                                @endphp
                                @foreach ($sucursales as $item)
                                    <tr>
                                        <td>{{$contador}}</td>
                                        <td>{{ $item->nombre }}</td>
                                        <td>{{ $item->razonSocial->nombre }}</td>
                                        <td>{{ $item->marcaComercial->nombreMarca }}</td>
                                        <td>{{ $item->estados->estado }}</td>
                                        <td>{{ $item->direccion }}</td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <button class="btn btn-xs btn-info" type="button" data-bs-toggle="modal" data-bs-target="#myModal"><i class="icofont icofont-edit-alt"></i></button>
                                                <button class="btn btn-xs btn-danger" type="button"><i class="icofont icofont-ui-delete"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    @php
                                        $contador++;
                                    @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection