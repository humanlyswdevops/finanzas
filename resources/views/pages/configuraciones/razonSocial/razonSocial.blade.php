@extends('layouts.simple.master')

@section('pageTitle', 'Razón social')

@section('title','Razón social')

@section('css')
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
    <h5>Razones Sociales</h5>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Configuraciones</li>
    <li class="breadcrumb-item active">Razón social</li>
@endsection

@section('content')
    <div class="conteiner-fluid">
        <div class="col-sm-12">
            <div class="card">
                <div class="ribbon ribbon-bookmark ribbon-secondary">Listado de razones sociales</div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-xs table-striped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Nombre</th>
                                    <th>Domicilio</th>
                                    <th>RFC</th>
                                    <th>Tipo de persona</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $contador = 1;   
                                @endphp
                                @foreach ($razonesSociales as $razonSocial)
                                    <tr>
                                        <td>{{$contador}}</td>
                                        <td>{{ $razonSocial->nombre }}</td>
                                        <td>{{ $razonSocial->domicilio }}</td>
                                        <td>{{ $razonSocial->rfc }}</td>
                                        <td>{{ $razonSocial->tipo }}</td>
                                        <td>
                                            <div class="btn-group" >
                                                <button class="btn btn-xs btn-info btnUpdateRazonSocial" type="button" 
                                                data-id="{{ $razonSocial->id }}"
                                                >
                                                    <i class="icofont icofont-edit-alt"></i>
                                                </button>
                                            </div>
                                        </td>
                                    </tr>
                                    @php
                                        $contador++;   
                                    @endphp



                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    @include('pages.configuraciones.razonSocial.modal.create')
@endsection



@section('js')
<script src=" {{ asset('js/configuraciones/razonSocial/razonSocial.js') }}"></script>
@endsection
