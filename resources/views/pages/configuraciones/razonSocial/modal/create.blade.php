<div class="modal fade" id="updateRazonSocialModal" tabindex="-1" role="dialog" aria-labelledby="myModal" aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-info">
                <h6 class="modal-title text-center col">Nueva Razón social</h6>
                <button class="btn-close" type="button" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>            
            <form id="updateRazonSocial" enctype="multipart/form-data">
                <input type="hidden" name="id" id="idRazonSocial">
                <div class="modal-body">
                    <p class="text-center">
                        Ingresa la informacoón correspondiente para comenzar a descardar los CFDI's.
                    </p>
                    <div class="row">
                        <div class="col-md-4 mb-3">
                            <label for="">Certificado:</label>
                            <input class="form-control" name="certificado" type="file"   required="">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="">Key:</label>
                            <input class="form-control" id="" name="key" type="file"   required="">
                        </div>
                        <div class="col-md-4 mb-3">
                            <label for="">Pem:</label>
                            <input class="form-control" id="" name="pem" type="file"   required="">
                        </div>
                        <div class="col-md-12 mb-3">
                            <label for="">Password</label>
                            <input class="form-control" id="" name="password" type="text"   required="">
                        </div>
                    </div>
                    <div class="col-md-12 text-center">
                        <button class="btn btn-primary" type="submit">Guardar Cambios</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@section('script')
<script src="{{asset('assets/js/form-validation-custom.js')}}"></script>
@endsection