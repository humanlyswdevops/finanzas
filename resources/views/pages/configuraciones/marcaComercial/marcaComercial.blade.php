@extends('layouts.simple.master')

@section('pageTitle','Marca comercial')

@section('title', 'Marca comercial')

@section('css')
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
    <h5>Marcas Comerciales</h5>
@endsection

@section('breadcrumb-items')
    <li class="breadcrumb-item">Configuraciones</li>
    <li class="breadcrumb-item active">Marca comercial</li>
@endsection

@section('content')
    <div class="container-fluid">
        <div class="col-sm-12">
            <div class="card">
                <div class="ribbon ribbon-bookmark ribbon-secondary">Listado de marcas comerciales</div>
                <div class="card-header p-2">
                    <button class="btn btn-outline-primary btn-sm f-right">
                        Nueva marca comercial
                    </button>
                </div>
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-xs table-stripped">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Marca Comercial</th>
                                    <th>Razón social</th>
                                    <th>Acciones</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $contador = 1;   
                                @endphp
                                @foreach ($marcasComerciales as $marcaComercial)
                                    <tr>
                                        <td>{{ $contador }}</td>
                                        <td>{{ $marcaComercial->nombreMarca }}</td>
                                        <td>{{ $marcaComercial->razonesSociales->nombre }}</td>
                                        <td>
                                            <div class="btn-group" role="group" aria-label="Basic example">
                                                <button class="btn btn-xs btn-info" type="button" data-bs-toggle="modal" data-bs-target="#myModal"><i class="icofont icofont-edit-alt"></i></button>
                                                <button class="btn btn-xs btn-danger" type="button"><i class="icofont icofont-ui-delete"></i></button>
                                            </div>
                                        </td>
                                    </tr>
                                    @php
                                        $contador++;
                                    @endphp
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection