<div class="modal" id="sesionModal">
    <div class="modal-dialog fadeIn  animated modal-dialog-centered " role="document">
       <div class="modal-content">
            <div class="modal-body p-0">
                <div class="header rounded p-1 pt-2 text-center">
                    <h5>Razón Social</h5>
                    <p class="mb-0">
                        Selecciona la razón social para establecer conexión con el SAT.
                    </p>
                </div>
                <div class="row p-4">
                    <form id="loginField" class="col-md-12">
                        <div class="d-flex">
                            <select id="select2" name="razonSocial">
                                <option></option>
                                @foreach ($razonesSociales as $razonSocial)
                                    <option value=" {{ encrypt($razonSocial->id) }}">{{ $razonSocial->nombre }}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-12 text-center mt-2">
                            <button type="submit" class="btn btn-primary  btn-sm">
                                Conectar 
                            </button>
                        </div>
                    </form>
                </div>
            </div>
       </div>
    </div>
 </div>