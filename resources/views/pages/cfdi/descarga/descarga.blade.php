@extends('layouts.simple.master')
@section('title', 'Sample Page')

@section('css')
@endsection

@section('style')
@endsection

@section('breadcrumb-title')
<h3>Descarga CDFI</h3>
@endsection

@section('breadcrumb-items')
<li class="breadcrumb-item">Pages</li>
<li class="breadcrumb-item active">Sample Page</li>
@endsection

@section('content')
<div class="conteiner-fluid">
    <div class="col-sm-12">
        <div class="card">
            <div class="ribbon ribbon-bookmark ribbon-secondary">Listado de razones sociales</div>
            <div class="card-header p-2 ">
                <button class="btn btn-outline-primary btn-sm f-right">
                        Nueva razón social
                </button>
            </div>
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-xs table-striped">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Domicilio</th>
                                <th>RFC</th>
                                <th>Tipo de persona</th>
                                <th>Acciones</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach ($razonesSociales as $razonSocial)
                                <tr>
                                    <td>{{ $razonSocial->id }}</td>
                                    <td>{{ $razonSocial->nombre }}</td>
                                    <td>{{ $razonSocial->domicilio }}</td>
                                    <td>{{ $razonSocial->rfc }}</td>
                                    <td>{{ $razonSocial->tipo }}</td>
                                    <td>
                                        <div class="btn-group" role="group" aria-label="Basic example">
                                            <button class="btn btn-xs btn-info" type="button" data-bs-toggle="modal" data-bs-target="#myModal"><i class="icofont icofont-edit-alt"></i></button>
                                            <button class="btn btn-xs btn-danger" type="button"><i class="icofont icofont-ui-delete"></i></button>
                                        </div>
                                    </td>
                                </tr>

                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

@include('pages/cfdi/descarga/modals/sesion')

@endsection

@section('js')

<script defer src="{{ asset('js/cfdi/descarga/descarga.js') }}"></script>

@endsection