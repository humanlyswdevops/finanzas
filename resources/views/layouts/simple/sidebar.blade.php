<div class="sidebar-wrapper">
	<div>
		<div class="logo-wrapper">
			<a href="{{route('/')}}"><img class="img-fluid for-light" src="{{asset('assets/images/logo/logo.png')}}" alt=""><img class="img-fluid for-dark" src="{{asset('assets/images/logo/logo_dark.png')}}" alt=""></a>
			<div class="back-btn"><i class="fa fa-angle-left"></i></div>
			<div class="toggle-sidebar"><i class="status_toggle middle sidebar-toggle" data-feather="grid"> </i></div>
		</div>
		<div class="logo-icon-wrapper"><a href="{{route('/')}}"><img class="img-fluid" src="{{asset('assets/images/logo/logo-icon.png')}}" alt=""></a></div>
		<nav class="sidebar-main">
			<div class="left-arrow" id="left-arrow"><i data-feather="arrow-left"></i></div>
			<div id="sidebar-menu">
				<ul class="sidebar-links" id="simple-bar">
					<li class="back-btn">
						<a href="{{route('/')}}"><img class="img-fluid" src="{{asset('assets/images/logo/logo-icon.png')}}" alt=""></a>
						<div class="mobile-back text-end"><span>Back</span><i class="fa fa-angle-right ps-2" aria-hidden="true"></i></div>
					</li>

					<li class="sidebar-list">
						<label class="badge badge-success">2</label>
						<a class="sidebar-link sidebar-title {{request()->route()->getPrefix() == '/dashboard' ? 'active' : '' }}" href="#">
							<i data-feather="home"></i>
							<span class="lan-3">{{ trans('lang.Dashboards') }}</span>
							<div class="according-menu">
								<i class="fa fa-angle-{{request()->route()->getPrefix() == '/dashboard' ? 'down' : 'right' }}"></i>
							</div>
						</a>
						<ul class="sidebar-submenu" style="display: {{ request()->route()->getPrefix() == '/dashboard' ? 'block;' : 'none;' }}">
							<li><a class="lan-4 {{ Route::currentRouteName()=='index' ? 'active' : '' }}" href="{{route('index')}}">{{ trans('lang.Default') }}</a></li>
                     		<li><a class="lan-5 {{ Route::currentRouteName()=='dashboard-02' ? 'active' : '' }}" href="{{route('dashboard-02')}}">{{ trans('lang.Ecommerce') }}</a></li>
						</ul>
					</li>
					<li class="sidebar-list">
						<a class="sidebar-link sidebar-title {{ request()->route()->getPrefix() == '/configuraciones' ? 'active' : '' }}" href="#" >
							<i data-feather="settings"></i>
							<span class="lan-6">Configuraciones</span>
							<div class="according-menu">
								<i class="fa fa-angle-{{ request()->route()->getPrefix() == '/configuraciones' ? 'down' : 'right'}}"></i>
							</div>
						</a>
						<ul class="sidebar-submenu" style="display: {{ request()->route()->getPrefix() == '/configuraciones' ? 'block;' : 'none;' }}">
							<li>
								<a class="lan-7 {{ Route::currentRouteName() == 'razon-social' ? 'active' : ''}}" href="{{ route('razon-social') }}">Razones sociales</a>
							</li>
							<li>
								<a class="lan-9 {{ Route::currentRouteName() == 'linea-comercial' ? 'active' : '' }}" href="{{ route('marca-comercial') }}">Marcas Comerciales</a>
							</li>
							<li>
								<a class="lan-8 {{ Route::currentRouteName() == 'sucursales' ? 'active' : '' }}" href="{{ route('sucursal') }}">Sucursales</a>
							</li>
						</ul>
					</li>

                    <li class="sidebar-list">
						<a class="sidebar-link sidebar-title {{ request()->route()->getPrefix() == '/CFDI' ? 'active' : '' }}" href="#" >
							<i data-feather="file-text"></i>
							<span class="lan-6">CFDI</span>
							<div class="according-menu">
								<i class="fa fa-angle-{{ request()->route()->getPrefix() == '/CFDI' ? 'down' : 'right'}}"></i>
							</div>
						</a>
						<ul class="sidebar-submenu" style="display: {{ request()->route()->getPrefix() == '/CFDI' ? 'block;' : 'none;' }}">
							<li>
								<a class="lan-7 {{ Route::currentRouteName() == 'descargaCfdi' ? 'active' : ''}}" href="{{ route('descargaCfdi') }}">Descarga</a>
							</li>
							<li>
								<a class="lan-9 {{ Route::currentRouteName() == 'linea-comercial' ? 'active' : '' }}" href="#">Descarga por dia</a>
							</li>
							<li>
								<a class="lan-8 {{ Route::currentRouteName() == 'sucursales' ? 'active' : '' }}" href="#">Visualización</a>
							</li>
						</ul>
					</li>

				</ul>
			</div>
			<div class="right-arrow" id="right-arrow"><i data-feather="arrow-right"></i></div>
		</nav>
	</div>
</div>
