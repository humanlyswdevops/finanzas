
$('.btnUpdateRazonSocial').on('click',function(){
    let id  = $(this).data('id');
    $('#idRazonSocial').val(id);
    $('#updateRazonSocialModal').modal('show');
});

updateRazonSocialModal

$('#updateRazonSocial').submit(function (e) { 
    e.preventDefault();
    updateRazonSocial();
});

function updateRazonSocial(){
    var formData = new FormData(document.getElementById("updateRazonSocial"));
    
    $.ajax({
        url: "updateRazonSocial",
        type: "post",
        dataType: "json",
        data: formData,
        cache: false,
        contentType: false,
        processData: false
    }).done(function(res){
        console.log(res);
    }); 
    
}