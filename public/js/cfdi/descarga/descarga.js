$(document).ready(function () {
    $('#sesionModal').modal('show');
    $('#select2').select2({
        placeholder:'Selecciona la razon social',
        dropdownParent: $('#sesionModal')
    });
});

$('#loginField').on('submit',function(e){
    e.preventDefault();
    
    $.ajax({
        type: "POST",
        url: "loginField",
        data: $(this).serialize(),
        dataType: "json",
    }).done(res=>{
        console.log(res);
    }).fail(err=>{
        console.log(err);
        
    });

})
