<?php

use App\Http\Controllers\Cfdi\DescargaController;
use App\Http\Controllers\Configuracion\MarcaComercialController;
use App\Http\Controllers\Configuracion\RazonSocialController;
use App\Http\Controllers\Configuracion\SucursalController;
use Illuminate\Support\Facades\Route;
use Illuminate\Http\Request;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Session;

Route::get('/', function () {
    return redirect()->route('index');
})->name('/');

Route::prefix('dashboard')->group(function () {
    Route::view('index', 'dashboard.index')->name('index');
    Route::view('dashboard-02', 'dashboard.dashboard-02')->name('dashboard-02');
});

Route::prefix('configuraciones')->group(function (){

    Route::get('razon-social', [ RazonSocialController::class, 'index' ])->name('razon-social');
    Route::post('updateRazonSocial',[RazonSocialController::class, 'updateRazonSocial']);

    Route::get('marca-comercial', [ MarcaComercialController::class, 'index' ])->name('marca-comercial');
    Route::get('sucursal', [ SucursalController::class, 'index' ])->name('sucursal');
});


Route::prefix('CFDI')->group(function(){
    Route::get('Descarga',[DescargaController::class,'index'])->name('descargaCfdi');
    Route::post('loginField',[DescargaController::class,'loginField']);
});
