<?php

namespace App\Http\Controllers\Configuracion;

use App\Models\Configuraciones\RazonSocial;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;

class RazonSocialController extends Controller
{
    public function index (){

        $razonSocial = RazonSocial::all();

        return view('pages/configuraciones/razonSocial/razonSocial',['razonesSociales' => $razonSocial, 'contador' => 1]);
    }

    public function saveRazonSocial (Request $request){

        $razonSocial = new RazonSocial();
        $razonSocial->idEstado = $request->idEstado;
        $razonSocial->idRegimenFiscal = $request->idRegimenFiscal;
        $razonSocial->nombre = $request->nombre;
        $razonSocial->alias = $request->alias;
        $razonSocial->rfc = $request->rfc;
        $razonSocial->tipoPersona = $request->tipoPersona;
        $razonSocial->calle = $request->calle;
        $razonSocial->colonia = $request->colonia;
        $razonSocial->codigoPostal = $request->codigoPostal;
        $razonSocial->activo = $request->activo;
        $razonSocial->save();
    }

    public function updateRazonSocial(Request $data){

        try {
            
            DB::beginTransaction();

                $validator = Validator::make($data->all(),[
                    'certificado' => 'required | file',
                    'key' => 'required | file',
                    'pem' => 'required | file',
                    'password' => 'required',
                ]);
                
                if($validator->fails() == true )

                $data = (Object) array(
                    'status' => 500,
                    'errors' => $validator->errors()
                );

                $nameCertificado = $this->fileUpload('Certificado',$data->certificado);
                $nameKey = $this->fileUpload('Key',$data->certificado);
                $namePem = $this->fileUpload('Pem',$data->certificado);

                
                $razonSocial = RazonSocial::find($data->id);
                $razonSocial->certificado = $nameCertificado;
                $razonSocial->key = $nameKey;
                $razonSocial->pem = $namePem;
                $razonSocial->pass = $data->password;
                $razonSocial->save();
                
                $data = (Object) array(
                    'status' => 200,
                    'antecedente' => $razonSocial
                );

            DB::commit();
        } catch (\Throwable $th) {
            DB::rollback();
            $data = array(
                'status' => 500,
                'errors' => $th->getMessage()
            );

        }
        return response()->json($data, $data->status);;

    }

    public function fileUpload($name,$file){
        $fileName = '';
        if ($file) {
            $fileName = $name.time();
            Storage::disk('sat')->put($fileName, File::get($file));
        } else {
            $fileName = 'null';
        }
        return $fileName;
    }
}
