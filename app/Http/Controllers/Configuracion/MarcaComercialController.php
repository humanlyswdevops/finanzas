<?php

namespace App\Http\Controllers\Configuracion;

use App\Http\Controllers\Controller;
use App\Models\Configuraciones\MarcaComercialModel;
use Illuminate\Http\Request;

class MarcaComercialController extends Controller
{
    public function index (){
        
        $marcaComercial = MarcaComercialModel::where('habilitado',1)->get();

        return view('pages/configuraciones/marcaComercial/marcaComercial',['marcasComerciales' => $marcaComercial]);
    }
}
