<?php

namespace App\Http\Controllers\Configuracion;

use App\Http\Controllers\Controller;
use App\Models\Configuraciones\Sucursal;
use Illuminate\Http\Request;

class SucursalController extends Controller
{
    public function index (){
        $sucursales = Sucursal::where('habilitado',1)->orderBy('nombre')->get();

        return view('pages/configuracion/sucursales/sucursal', [ 'sucursales' => $sucursales ]);
    }
}
