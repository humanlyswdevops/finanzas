<?php
namespace App\Http\Controllers\Cfdi\lib;

use App\Http\Controllers\Controller;



/**
 * Clase para trabajar con certificados.
 * @version 10.1b
 *
 * Este programa se distribuye con licencia de uso comercial
 *
 *    - Usted puede modificar el programa para fines comerciales siempre que haya
 *      pagado el precio especificado por www.globalsoftware.com.mx a través de sus
 *      canales de venta.
 *
 * @copyright Copyright (c) 2020, Global Software.
 * @link      http://www.globalsoftware.com.mx
 *
 * EL USO DEL PROGRAMA Y LOS RESULTADOS OBTENIDOS POR SU USO SON RESPONSABILIDAD
 * UNICAMENTE DEL USUARIO, TODA VEZ QUE EL USUARIO TIENE A SU DISPOSICION EL CODIGO FUENTE.
 *
 */


class UtilCertificado extends Controller{
    private $cerFileContent;
    private $keyFileContent;
    private $keyPemFileContent;
    //private $keyPassword="DIAGNOSTICO";
    private $keyPassword;
    private static $openSslFile;


    public function loadFiles($razonSocial){

        $cerFileContent = file_get_contents($razonSocial->certificado);

        if(!empty($cerFileContent)){
            $this->cerFileContent = $cerFileContent;

            $this->keyFileContent = file_get_contents($razonSocial->key);
            $this->keyPemFileContent = $this->getKeyPemContentAEL();

            if($this->keyPemFileContent){
                $this->keyPassword = $razonSocial->keyPassword;
                return true;
            }
        }

        return false;
    }

    public function loadData($cerFileContent, $keyPemFileContent){
        $this->cerFileContent = $cerFileContent;
        $this->keyPemFileContent = $keyPemFileContent;
        return true;
    }

    public static function establecerRutaOpenSSL($ruta=null) {
        if($ruta) {
            // utilizar la ruta especificada

            self::$openSslFile = $ruta;
            if(file_exists(self::$openSslFile)) return;

            throw new Exception('Ruta OpenSSL no válida', 2);
        }else{
            // intentar obtener la ruta automáticamente según el SO

            $os = strtoupper(php_uname('s'));
            if(strpos($os, 'WIN') === 0) { // windows
                self::$openSslFile = 'C:\OpenSSL\bin\openssl.exe';
                if(file_exists(self::$openSslFile)) return;

                self::$openSslFile = 'C:\OpenSSL\bin\openssl.exe';
                if(file_exists(self::$openSslFile)) return;
            }else{ // unix
                self::$openSslFile = '/usr/bin/openssl';
                if(file_exists(self::$openSslFile)) return;
            }

            throw new Exception('Ruta OpenSSL no configurada', 1);
        }
    }

    public function firmarCadena($cadena, $algo){
        $resultado = null;
        $pKeyId = openssl_pkey_get_private($this->keyPemFileContent);
        $signOk = @openssl_sign(
            $cadena,
            $resultado,
            $pKeyId,
            $algo
        );
        openssl_free_key($pKeyId);

        if($signOk){
            return base64_encode($resultado);
        }

        return null;
    }

    public function getRFC(){
        $d = openssl_x509_parse(
            $this->getCerPemContent(),
            true
        );

        if($d) {
            $rfcs = explode(
                '/',
                str_replace(' ', '', $d['subject']['x500UniqueIdentifier'])
            );
            return strtoupper($rfcs[0]);
        }

        return null;
    }

    public function getRangoValidez(){
        $d = openssl_x509_parse(
            $this->getCerPemContent(),
            true
        );

        if($d) {
            return array(
                'from' => $d['validFrom_time_t'],
                'to'   => $d['validTo_time_t']
            );
        }

        return null;
    }

    public function getNombre(){
        $d = openssl_x509_parse(
            $this->getCerPemContent(),
            true
        );

        if($d && !empty($d['subject']['CN'])) {
            return $d['subject']['CN'];
        }

        return null;
    }

    public function getNumeroCertificado(){
        $d = openssl_x509_parse(
            $this->getCerPemContent(),
            true
        );

        if($d && !empty($d['serialNumberHex'])) {
            $hex = $d['serialNumberHex'];
            $num = '';
            for($i=0;$i<strlen($hex);$i+=2) {
                $num .= chr(hexdec(substr($hex, $i, 2)));
            }
            return $num;
        }

        if($d && !empty($d['serialNumber'])) {
            $number = $d['serialNumber'];
            $hexvalues = array('0','1','2','3','4','5','6','7','8','9','A','B','C','D','E','F');
            $hexval = '';
            while($number != '0'){
                $hexval = $hexvalues[bcmod($number,'16')].$hexval;
                $number = bcdiv($number,'16',0);
            }
            $number = '';
            $len = strlen($hexval);
            for($i=0; $i<$len;$i+=2){
                $number .=  substr($hexval, $i+1, 1);
            }

            return $number;
        }

        return null;
    }

    public function getTipoCertificado(){
        $d = openssl_x509_parse(
            $this->getCerPemContent(),
            true
        );

        if($d) {
            $keyUsage = explode(',', str_replace(' ', '', $d['extensions']['keyUsage']));
            $count = count($keyUsage);
            if($count > 0 && in_array('DigitalSignature',$keyUsage) && in_array('NonRepudiation',$keyUsage)){
                if($count == 2){
                    return 'CSD';
                }elseif($count == 4){
                    return 'FIEL';
                }
            }
        }

        return null;
    }

    public function getCerPemContent(){
        return self::der2pem($this->cerFileContent);
    }

    public function getKeyPemFileContent() {
        return $this->keyPemFileContent;
    }

    public function getKeyPassword() {
        return $this->keyPassword;
    }

    public function getCerFileContent() {
        return $this->cerFileContent;
    }

    public function getKeyFileContent() {
        return $this->keyFileContent;
    }

    private function getKeyPemContentAEL(){
        $res = "-----BEGIN RSA PRIVATE KEY-----
MIIEpQIBAAKCAQEA8F6/gXWrrUwzpWbbd2ums+u348dHc8OYDXNWlLNI8WmYjHkT
i5QP/Y2GMXf1O5+MErWIQjOvt2H64OKx3pn4V2q5A45nEgtXFTi47NXoVFaICvls
E066JbQz7YDgqlgLYeb8gVKYdo3aFAAha7yk+OX1zljz3TwbkhAq9KfI+m/YmFsg
qBD6OjPOGwGXD+adIdxu4Q8EojVH+i7nj192rIG5re7o6L57Pivs78wrHPMjmjDA
g/v9vhLIhIEflWPjtbedRR1CNoqCmeWELNWiAIbg30t4cUPyIXzFICF45fpAue0A
XzggqKozxMefC2QE+uS9iBJbcMtk1It6KDqLXwIDAQABAoIBAF6qwjunb2TCNTj4
SIlA8Ze8x2DDNGXoa7MocLoB3kGN+BGjAcqgFqgim+5Rs1i3HoCj1DVWi+bqhNcb
ETh9ZWxwUMuQwxE9PB/UhihomqbUqaHA2ArGn9OHpEfu0Qzlvh3hziLUt81BUtk7
r+3xjvuj/eo16F2Cdc1R5skSZ7oRnVPaa/bANCqQzyZ8ED9Qzk4MNqktXCHrQOFB
y6TucGPqozMieVsZsPlKjSAzL2csqwtzuXTfs45+HaBOorDmHefGPdv643lDwU6T
ysda5ebZGXKhfUedCUZBC8ho8PZu8jl2YRks3pzIUdzHl2CM1ckUhpsqIj94COGp
V0QHMLECgYEA+NuQ5bf3iDY1hS3XrsGQLR9UpoVGm7ya/VHQOfga+ttr87Rzed+X
xQDq3wf+Wjia2utbqostnHW+ddRskMevDf30MYtjJSzQ+AV32AGxqga8SpwU/9RK
Ky8/9DP/FqMhCZGqTY7Y/FDRY85rfivfInKEOQuPVWIxu09vKV4uRVkCgYEA90TS
PvkQV1uNR3SijkOszK1013hWWMbm4eLZ36ky6k1zIff1Pq224gI5s2TcMQQfSp2l
dZPyJp/DFDVjkQ9CDYdmh7PjPWtoSQvutJj6emIpPmtVLc8HrHi3SUuzDDn7hCOA
8yXtQ0B67gD1Pq0Sn5DGUlKrVlbcoHKhdXut53cCgYEA894+5U7drMbqtcMRzHh0
8DL2wmRyqCUwgTQOaPBgiBpEdMMOMo1RbkYS5FMyB3nXDtd7senH3b5o0ZFEfrCg
NIb8KR0eII0FjzfUlII8d5E4Lak8zuNbd2xLMOVPggCEXJs5l913d/M+7p6XKkKV
3Gz6c+iHIrYiZBtYpOJdV6kCgYEA88R8RSrDSmvuVvCMRRZAp7OlCbsP3QuRmEVN
J2slz8k05NsPgpA9SIiLG+kmDoiS1Luf389PnXZJGoYbt2lEZBQFJ2lXcDiQIdtK
o+5gmQBmyquD2NpOJWEIyJCBXl9XLwjVWmAcjP1nc3WsUPLM9lQCwCvR0BdoACGi
PTEF27cCgYEA8r42EzHS0X2BRX6f7XtG/371JJLJhTpNhTcSPvpTMlobsAt0rfnB
yVo+uGHv52tY0Am1/CIiA7V3M4HUYN+ySWu/Lk02JYKOpN8/oTyvJdzVZB0extye
m4RnShoEQ6H/55CQ8XuuEL2VgPU4Ju6ujaII/7VNQG7Bf5QXjvC+zTY=
-----END RSA PRIVATE KEY-----";

        if(!empty($res)){
            return $res;
        }

        return null;
    }

    private function getKeyPemContentLABT(){
        $res = "-----BEGIN PRIVATE KEY-----
MIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCL+mX8R7ccpavj
s1LVDBOFLrwrPsLPTajN3Ne+PltQODuh6I8IeyG4kZqcRWfUZhllQR9KmFjPnf9t
sdOdTZNcgIAztOSEewNVa0S64EZRi0hZ7vlngJ3rzhwF6QLKIQ5kyYc4r54rXC19
IM+t9Zzp5J7NQqFrXNXwy5UaqdKXtuahHEPbUNrJqTiwu5XF9le3m6VJvSB5BS9t
MR5EzfCyemGt92kiL1zQKaJyOyMVaKj3DagYN38JCTfeprXccZghGNMK16Mhtm91
v7wDGqZUKYBnIrTK+v+/fRvdqGcefeXyp93hzlBam6dW9F7EqZDz6hDUJRSpKmbA
L4zTRJ+bAgMBAAECggEBAIWoeQ1lpREzmGf56NQJjfIT6BUa/oVX0CGSCGddv29f
4oD9QOaGPHKyhYFGPJhxTTqD7k8aHWNRqH6j2D5Rvq4K/V9iEPn3Q/2iSaRvVnWy
m0lUiM0t4J0y10tFbSZ4Xfga/oVAG7WndMAQPn4LEuO7K28vela79ITSv/MPyUiX
Ly4nXJ6GsJmISOg7dZWHH8NFwGsa8XoiisxbIIHQE929nnr+MlImKRGrGi/oHIie
x+EY0Nj1ODqbJEco+T2oVJlMymGfpH71r0gtIvdw4/7PdHUXOq4qM6BWipVrPvpJ
lORGSKBGQTDJjDVxD1PzDraL2tjbIZo0QjKCjQ4AXuECgYEA0jOHsimPL3k/2yc8
ey0M5I69dDVy4wOBgTp7nbw2Q70wUIMW0duogFHJ4TCsnGBKkOeTg5nDxgwNop54
H2S+0oRIW0YtaYoXKCTuZRUsAfWbV3p1yIY3WKMIBL/TuYzYxFqAdoAOiQq48Sn7
XQx8dfAJL121Oz6f2cXgQkS553MCgYEAqnoBmssACuSSbZAnrI5vsxUUA0YrNlxr
tlGlB1MfQeJCtfI7ZS1O/bZgsC9LNGM0TB0OWQOHUCnxuoBMK+WuADp4wqeJpQRG
jeRyvsdMYhTr5LgI/lg7usURJ3bCY9xwz+InRO0Qpkk9LHB7zkrynpBGjuv+MbVl
n+hEDR4TzTkCgYB6YwJpG4WA8KnbYfb3CP3uvaVVWiFwSARJR9Hdv04hFrBg0FKj
EbAENprCgOKfkfkYcZ0UDJ1OXIo72uS58qCgVeEDM9MRFhPxjb84H6Gl6mjuACFA
BdWHyDTM/xu5d2ZmvQL6/Ah6hkG0OOtf+/sjeAUBDSCORCIyW+H886MCPQKBgBbO
u+g5wIe0jOKbprMvW0JaT5MgkCd1tfNueOXB+SyXWNV+0vN2XFsVS1pG+YjIvPZx
7Ll7/156Phz1jTtSPzVInV9tirzJFadmKPPdNhrpMaYn+QwOQe967hCZkuhJVc56
M0bFcLTOl6Caa/XtkQglxUrZx+henHoIWRJoM74BAoGAXr3/5h0AHFlPtnUCuZhM
JIASsoZ55+3Dz0NqmGqLQtUs/tWqgFjhYkDT3vVdFKFS97TFFgH9LHpD+3mp05Oc
sG/zP/HL4OjD+Qx+FBdlp2XZsgfRg41zslz5pj3DnW+rFKjD2nXc86kXUIfFnqb9
zdXMHkWiYt+H6CBDCwxuMUQ=
-----END PRIVATE KEY-----";

        if(!empty($res)){
            return $res;
        }

        return null;
    }

    private static function der2pem($der_data) {
        return '-----BEGIN CERTIFICATE-----'.PHP_EOL
            .chunk_split(base64_encode($der_data), 64, PHP_EOL)
            .'-----END CERTIFICATE-----'.PHP_EOL;
    }

    public function toBase64(){
        return str_replace(
            array('\n', '\r'),
            '',
            base64_encode($this->cerFileContent)
        );
    }
}
