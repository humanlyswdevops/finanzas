<?php

namespace App\Http\Controllers\Cfdi;

use App\Http\Controllers\Cfdi\lib\DescargaMasivaCfdi;
use App\Http\Controllers\Cfdi\lib\UtilCertificado;
use App\Http\Controllers\Controller;
use App\Models\Configuraciones\RazonSocial;
use Illuminate\Http\Request;

class DescargaController extends Controller
{
    public function index(){
        $razonesSociales = $this->getRazonSocial();
        return \view('pages/cfdi/descarga/descarga',\compact('razonesSociales'));
    }

    public function getRazonSocial(){
        return RazonSocial::all();
    }

    public function loginField(Request $data){

        $idRazonSocial = \decrypt($data->razonSocial);
        $razonSocial = RazonSocial::find($idRazonSocial);
        $certificado = new UtilCertificado();
        $loadCertificado = $certificado->loadFiles($razonSocial);
        if($loadCertificado) {
            $descargaCfdi = new DescargaMasivaCfdi();
            $sessionGet = $descargaCfdi->iniciarSesionFiel($certificado);
            if($sessionGet) {
                $data = array(
                    'code' => 200,
                    'mensaje' => 'Se ha iniciado la sesión',
                    'sesion' => $descargaCfdi->obtenerSesion()
                );
            }else{
                $data = array(
                    'code' => 400,
                    'mensaje' => 'Ha ocurrido un error al iniciar sesión. Intente nuevamente',
                );
            }
        }else{
            $data = array(
                'code' => 400,
                'mensaje' => 'Verifique que los archivos corresponden con la contraseña e intente nuevamente',
            );
        }

        return response()->json($data, $data['code']);

    }

    public function cfdiRecibidos(){
        $filtros = new BusquedaRecibidos();
        $descargaCfdi = new DescargaMasivaCfdi();
        $data = (Object) $this->request->getPost();
        $filtros->establecerFecha($data->anio, $data->mes, $data->dia);
        $xmlInfoArr = $descargaCfdi->buscar($filtros);
        if($xmlInfoArr){
            $items = array();
            foreach ($xmlInfoArr as $xmlInfo) {
                $items[] = (array)$xmlInfo;
            }
            $data = array(
                'items' => $items,
                'sesion' => $descargaCfdi->obtenerSesion()
            );
        }
        else{
            $data = array(
                'mensaje' => 'No se han encontrado CFDIs',
                'sesion' => $descargaCfdi->obtenerSesion()
            );
        }

        return $this->response->setJson($data);
    }



}
