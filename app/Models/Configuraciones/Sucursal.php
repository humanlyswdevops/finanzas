<?php

namespace App\Models\Configuraciones;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Sucursal extends Model
{
    use HasFactory;

    protected $table = "sucursales";
    protected $idSucursal = 'id';

    public $timestamps = false;

    public function razonSocial()
    {
        return $this->hasOne(RazonSocial::class, 'id', 'idRazonSocial');
    }

    public function marcaComercial()
    {
        return $this->hasOne(MarcaComercialModel::class, 'id', 'idMarcaComercial');
    }

    public function estados()
    {
        return $this->hasOne(Estado::class, 'id', 'idEstado');
    }
}
