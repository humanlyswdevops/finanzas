<?php

namespace App\Models\Configuraciones;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RazonSocial extends Model
{
    use HasFactory;

    protected $table = "razonesSociales";

    protected $idRazonSocial = 'id';

    public $timestamps = false;
}
