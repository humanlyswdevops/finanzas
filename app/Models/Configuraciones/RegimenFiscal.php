<?php

namespace App\Models\Configuraciones;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RegimenFiscal extends Model
{
    use HasFactory;

    protected $table = "catRegimenFiscal";

    public $timestamps = false;

    protected $fillable = ['id','clave','descripcion','fisica','moral'];

    public function obtenerRigimen(){
        return RegimenFiscal::all();
    }

    public function obtenerRegimenPersona($tipoPersona){
        if ($tipoPersona == "MORAL") {
            
            return RegimenFiscal::where('moral', 'Sí');
        }
        else{
            return RegimenFiscal::where('fisica', 'Sí');
        }
    }

    public function obtenerRegimenId($idRegimenFiscal){
        
        return RegimenFiscal::find($idRegimenFiscal);
    }
}   
