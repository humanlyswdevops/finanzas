<?php

namespace App\Models\Configuraciones;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class MarcaComercialModel extends Model
{
    use HasFactory;

    protected $table = "marcaComercial";

    protected $idMarcaComercial = 'id';

    public $timestamps = false;

    public function razonesSociales()
    {
        return $this->hasOne(RazonSocial::class, 'id','idRazonSocial');
    }
}
